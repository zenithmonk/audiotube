# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the audiotube package.
#
# Raghavendra Kamath <raghu@raghukamath.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: audiotube\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-04-07 00:38+0000\n"
"PO-Revision-Date: 2021-09-17 20:09+0530\n"
"Last-Translator: Raghavendra Kamath <raghu@raghukamath.com>\n"
"Language-Team: kde-hindi\n"
"Language: hi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Lokalize 21.08.1\n"

#: asyncytmusic.cpp:65
#, kde-format
msgid ""
"Running with untested version of ytmusicapi %1. If you experience errors, "
"please report them to your distribution."
msgstr ""

#: contents/ui/AlbumPage.qml:71 contents/ui/LibraryPage.qml:39
#: contents/ui/LibraryPage.qml:65 contents/ui/LibraryPage.qml:91
#: contents/ui/LibraryPage.qml:235 contents/ui/LibraryPage.qml:261
#: contents/ui/LibraryPage.qml:285 contents/ui/LocalPlaylistPage.qml:72
#: contents/ui/LocalPlaylistPage.qml:93 contents/ui/PlaybackHistory.qml:67
#: contents/ui/PlaybackHistory.qml:89 contents/ui/PlaylistPage.qml:66
#: contents/ui/PlaylistPage.qml:87
#, kde-format
msgid "Play"
msgstr "बजाएँ"

#: contents/ui/AlbumPage.qml:78 contents/ui/ArtistPage.qml:83
#: contents/ui/LibraryPage.qml:46 contents/ui/LibraryPage.qml:71
#: contents/ui/LibraryPage.qml:101 contents/ui/LibraryPage.qml:242
#: contents/ui/LibraryPage.qml:267 contents/ui/LibraryPage.qml:294
#: contents/ui/LocalPlaylistPage.qml:67 contents/ui/LocalPlaylistPage.qml:99
#: contents/ui/PlaybackHistory.qml:55 contents/ui/PlaybackHistory.qml:102
#: contents/ui/PlaylistPage.qml:59 contents/ui/PlaylistPage.qml:94
#, kde-format
msgid "Shuffle"
msgstr "फेंटें"

#: contents/ui/AlbumPage.qml:86 contents/ui/LibraryPage.qml:76
#: contents/ui/LibraryPage.qml:111 contents/ui/LibraryPage.qml:272
#: contents/ui/LibraryPage.qml:302 contents/ui/LocalPlaylistPage.qml:104
#: contents/ui/PlaybackHistory.qml:114 contents/ui/PlaylistPage.qml:102
#, kde-format
msgid "Append to queue"
msgstr ""

#: contents/ui/AlbumPage.qml:91 contents/ui/ArtistPage.qml:91
#: contents/ui/PlaylistPage.qml:112
#, kde-format
msgid "Open in Browser"
msgstr ""

#: contents/ui/AlbumPage.qml:96 contents/ui/ArtistPage.qml:96
#: contents/ui/PlaylistPage.qml:117
#, kde-format
msgid "Share"
msgstr ""

#: contents/ui/AlbumPage.qml:106
#, kde-format
msgid "Album • %1"
msgstr ""

#: contents/ui/AlbumPage.qml:160 contents/ui/ArtistPage.qml:201
#: contents/ui/LocalPlaylistPage.qml:168 contents/ui/PlaybackHistory.qml:195
#: contents/ui/PlaylistPage.qml:189 contents/ui/SearchPage.qml:148
#, kde-format
msgid "More"
msgstr ""

#: contents/ui/ArtistPage.qml:77
#, kde-format
msgid "Radio"
msgstr "रेडियो"

#: contents/ui/ArtistPage.qml:105
#, fuzzy, kde-format
#| msgid "Artists"
msgid "Artist"
msgstr "कलाकार"

#: contents/ui/ArtistPage.qml:129 contents/ui/SearchPage.qml:74
#, kde-format
msgid ""
"Video playback is not supported yet. Do you want to play only the audio of "
"\"%1\"?"
msgstr ""
"वीडियो चलाना अभी तक समर्थित नहीं है। क्या आप केवल \"%1\" का ध्वनी चलाना चाहते हैं?"

#: contents/ui/ArtistPage.qml:148 contents/ui/SearchPage.qml:28
#, kde-format
msgid "Albums"
msgstr "एल्बम्स"

#: contents/ui/ArtistPage.qml:150
#, kde-format
msgid "Singles"
msgstr "एक गीत"

#: contents/ui/ArtistPage.qml:152 contents/ui/SearchPage.qml:34
#, kde-format
msgid "Songs"
msgstr "गीत"

#: contents/ui/ArtistPage.qml:154 contents/ui/SearchPage.qml:36
#, kde-format
msgid "Videos"
msgstr "वीडियो"

#: contents/ui/ConfirmationMessage.qml:16
#, kde-format
msgid "OK"
msgstr ""

#: contents/ui/ConfirmationMessage.qml:21
#, kde-format
msgid "Cancel"
msgstr ""

#: contents/ui/dialogs/AddPlaylistDialog.qml:17
#, fuzzy, kde-format
#| msgid "Add to Playlist"
msgid "Add playlist"
msgstr "गीत-सूची में जोड़ें"

#: contents/ui/dialogs/AddPlaylistDialog.qml:23
#: contents/ui/dialogs/RenamePlaylistDialog.qml:24
#, fuzzy, kde-format
#| msgid "Playlists"
msgid "Playlist Title"
msgstr "गीत-सूचीयाँ"

#: contents/ui/dialogs/AddPlaylistDialog.qml:28
#: contents/ui/dialogs/RenamePlaylistDialog.qml:30
#, fuzzy, kde-format
#| msgid "Playlists"
msgid "Playlist Description"
msgstr "गीत-सूचीयाँ"

#: contents/ui/dialogs/ImportPlaylistDialog.qml:17
#, fuzzy, kde-format
#| msgid "Add to Playlist"
msgid "Import playlist"
msgstr "गीत-सूची में जोड़ें"

#: contents/ui/dialogs/ImportPlaylistDialog.qml:23
#, fuzzy, kde-format
#| msgid "Playlists"
msgid "Playlist URL (Youtube)"
msgstr "गीत-सूचीयाँ"

#: contents/ui/dialogs/PlaylistDialog.qml:20
#, fuzzy, kde-format
#| msgid "Add to Playlist"
msgid "Add Song to Playlist"
msgstr "गीत-सूची में जोड़ें"

#: contents/ui/dialogs/PlaylistDialog.qml:29
#: contents/ui/LocalPlaylistsPage.qml:107
#: contents/ui/LocalPlaylistsPage.qml:133
#: contents/ui/LocalPlaylistsPage.qml:151
#, fuzzy, kde-format
#| msgid "Playlists"
msgid "New Playlist"
msgstr "गीत-सूचीयाँ"

#: contents/ui/dialogs/RenamePlaylistDialog.qml:18
#, fuzzy, kde-format
#| msgid "Playlists"
msgid "Rename playlist"
msgstr "गीत-सूचीयाँ"

#: contents/ui/LibraryPage.qml:32 contents/ui/LibraryPage.qml:135
#: contents/ui/NavigationBar.qml:47 contents/ui/NavigationBar.qml:53
#: contents/ui/Sidebar.qml:138 contents/ui/Sidebar.qml:143
#, kde-format
msgid "Favourites"
msgstr ""

#: contents/ui/LibraryPage.qml:130 contents/ui/LibraryPage.qml:322
#: contents/ui/LibraryPage.qml:424
#, kde-format
msgid "Show All"
msgstr ""

#: contents/ui/LibraryPage.qml:160
#, kde-format
msgid "No Favourites Yet"
msgstr ""

#: contents/ui/LibraryPage.qml:228
#, kde-format
msgid "Most played"
msgstr ""

#: contents/ui/LibraryPage.qml:327 contents/ui/NavigationBar.qml:60
#: contents/ui/NavigationBar.qml:66 contents/ui/Sidebar.qml:159
#: contents/ui/Sidebar.qml:164
#, kde-format
msgid "Played Songs"
msgstr ""

#: contents/ui/LibraryPage.qml:353
#, kde-format
msgid "No Songs Played Yet"
msgstr ""

#: contents/ui/LibraryPage.qml:412 contents/ui/LocalPlaylistsPage.qml:20
#: contents/ui/LocalPlaylistsPage.qml:84 contents/ui/NavigationBar.qml:74
#: contents/ui/SearchPage.qml:32 contents/ui/Sidebar.qml:179
#, kde-format
msgid "Playlists"
msgstr "गीत-सूचीयाँ"

#: contents/ui/LibraryPage.qml:451
#, fuzzy, kde-format
#| msgid "Playlists"
msgid "No Playlists Yet"
msgstr "गीत-सूचीयाँ"

#: contents/ui/LibraryPage.qml:474 contents/ui/LibraryPage.qml:496
#: contents/ui/LocalPlaylistsPage.qml:38 contents/ui/LocalPlaylistsPage.qml:60
#, kde-format
msgid "Rename"
msgstr ""

#: contents/ui/LibraryPage.qml:483 contents/ui/LibraryPage.qml:504
#: contents/ui/LocalPlaylistsPage.qml:47 contents/ui/LocalPlaylistsPage.qml:68
#, kde-format
msgid "Delete"
msgstr ""

#: contents/ui/LocalPlaylistPage.qml:22
#, fuzzy, kde-format
#| msgid "Playlists"
msgid "Remove from Playlist"
msgstr "गीत-सूचीयाँ"

#: contents/ui/LocalPlaylistPage.qml:80
#, kde-format
msgid "This playlist is still empty"
msgstr ""

#: contents/ui/LocalPlaylistsPage.qml:115
#: contents/ui/LocalPlaylistsPage.qml:138
#: contents/ui/LocalPlaylistsPage.qml:156 contents/ui/PlaylistPage.qml:107
#, fuzzy, kde-format
#| msgid "Playlists"
msgid "Import Playlist"
msgstr "गीत-सूचीयाँ"

#: contents/ui/main.qml:126 main.cpp:72
#, kde-format
msgid "AudioTube"
msgstr "ओडियो-ट्यूब"

#: contents/ui/main.qml:279
#, fuzzy, kde-format
#| msgid "No media playing"
msgid "No song playing"
msgstr "कोई मीडिया नहीं बजा रहा है"

#: contents/ui/MaximizedPlayerPage.qml:99
#, kde-format
msgid "Close Maximized Player"
msgstr ""

#: contents/ui/MaximizedPlayerPage.qml:206
#: contents/ui/MinimizedPlayerControls.qml:112
#, kde-format
msgid "No media playing"
msgstr "कोई मीडिया नहीं बजा रहा है"

#: contents/ui/MaximizedPlayerPage.qml:433
#, fuzzy, kde-format
#| msgid "Playlists"
msgid "Remove from Favourites"
msgstr "गीत-सूचीयाँ"

#: contents/ui/MaximizedPlayerPage.qml:433
#, fuzzy, kde-format
#| msgid "Add to Playlist"
msgid "Add to Favourites"
msgstr "गीत-सूची में जोड़ें"

#: contents/ui/MaximizedPlayerPage.qml:458
#, kde-format
msgid "Open Volume Drawer"
msgstr ""

#: contents/ui/MaximizedPlayerPage.qml:568
#, kde-format
msgid "Unmute Audio"
msgstr ""

#: contents/ui/MaximizedPlayerPage.qml:568
#, kde-format
msgid "Mute Audio"
msgstr ""

#: contents/ui/MaximizedPlayerPage.qml:607
#, kde-format
msgid "%1%"
msgstr ""

#: contents/ui/MaximizedPlayerPage.qml:637
#, kde-format
msgid "Hide Lyrics"
msgstr ""

#: contents/ui/MaximizedPlayerPage.qml:637
#, kde-format
msgid "Show Lyrics"
msgstr ""

#: contents/ui/MaximizedPlayerPage.qml:657 contents/ui/SongMenu.qml:195
#: contents/ui/SongMenu.qml:281
#, kde-format
msgid "Share Song"
msgstr ""

#: contents/ui/MaximizedPlayerPage.qml:698
#, fuzzy, kde-format
#| msgid "Add to Playlist"
msgid "Add to a local playlist"
msgstr "गीत-सूची में जोड़ें"

#: contents/ui/MaximizedPlayerPage.qml:735
#, kde-format
msgid "Hide Queue"
msgstr ""

#: contents/ui/MaximizedPlayerPage.qml:735
#, kde-format
msgid "Show Queue"
msgstr ""

#: contents/ui/MaximizedPlayerPage.qml:966
#: contents/ui/MaximizedPlayerPage.qml:1210
#, kde-format
msgid "Remove Track"
msgstr "गाना हटाएँ"

#: contents/ui/MaximizedPlayerPage.qml:1001
#, fuzzy, kde-format
#| msgid "Clear"
msgid "Clear Queue"
msgstr "साफ करें"

#: contents/ui/MaximizedPlayerPage.qml:1026
#, fuzzy, kde-format
#| msgid "Shuffle"
msgid "Shuffle Queue"
msgstr "फेंटें"

#: contents/ui/MaximizedPlayerPage.qml:1059
#, kde-format
msgid "Upcoming Songs"
msgstr ""

#: contents/ui/NavigationBar.qml:23 contents/ui/Sidebar.qml:108
#, kde-format
msgid "Library"
msgstr ""

#: contents/ui/NavigationBar.qml:35 contents/ui/Sidebar.qml:119
#, kde-format
msgid "Search"
msgstr "खोजें"

#: contents/ui/PlaybackHistory.qml:17
#, kde-format
msgid "Unknown list of songs"
msgstr ""

#: contents/ui/PlaybackHistory.qml:135
#, kde-format
msgid "No songs here yet"
msgstr ""

#: contents/ui/PlaylistPage.qml:76
#, kde-format
msgid "Playlist successfully imported"
msgstr ""

#: contents/ui/PlaylistPage.qml:128
#, fuzzy, kde-format
#| msgid "Playlists"
msgid "Playlist"
msgstr "गीत-सूचीयाँ"

#: contents/ui/SearchHistoryPage.qml:82 contents/ui/SearchWithDropdown.qml:393
#, kde-format
msgid "remove from search history"
msgstr ""

#: contents/ui/SearchPage.qml:16
#, kde-format
msgid "Previous Searches:"
msgstr ""

#: contents/ui/SearchPage.qml:30
#, kde-format
msgid "Artists"
msgstr "कलाकार"

#: contents/ui/SearchPage.qml:38
#, kde-format
msgid "Top Results"
msgstr ""

#: contents/ui/SearchPage.qml:41 playlistimporter.cpp:22
#, kde-format
msgid "Unknown"
msgstr "अज्ञात"

#: contents/ui/SearchWithDropdown.qml:339
#, kde-format
msgid "No matching previous searches"
msgstr ""

#: contents/ui/ShareMenu.qml:41
#, kde-format
msgid "Share to"
msgstr ""

#: contents/ui/ShareMenu.qml:49 contents/ui/ShareMenu.qml:102
#, kde-format
msgid "Copy Link"
msgstr ""

#: contents/ui/ShareMenu.qml:54 contents/ui/ShareMenu.qml:107
#, kde-format
msgid "Link copied to clipboard"
msgstr ""

#: contents/ui/Sidebar.qml:200
#, kde-format
msgid "About"
msgstr ""

#: contents/ui/Sidebar.qml:214
#, kde-format
msgid "Collapse Sidebar"
msgstr ""

#: contents/ui/SongMenu.qml:108 contents/ui/SongMenu.qml:217
#, kde-format
msgid "Play Next"
msgstr "अगला बजाएँ"

#: contents/ui/SongMenu.qml:118 contents/ui/SongMenu.qml:223
#, kde-format
msgid "Add to queue"
msgstr ""

#: contents/ui/SongMenu.qml:130 contents/ui/SongMenu.qml:233
#, kde-format
msgid "Remove Favourite"
msgstr ""

#: contents/ui/SongMenu.qml:130 contents/ui/SongMenu.qml:233
#, kde-format
msgid "Add Favourite"
msgstr ""

#: contents/ui/SongMenu.qml:149 contents/ui/SongMenu.qml:247
#, kde-format
msgid "Remove from History"
msgstr ""

#: contents/ui/SongMenu.qml:164 contents/ui/SongMenu.qml:256
#, fuzzy, kde-format
#| msgid "Add to Playlist"
msgid "Add to playlist"
msgstr "गीत-सूची में जोड़ें"

#: main.cpp:74
#, fuzzy, kde-format
#| msgid "Find music on YouTube Music"
msgctxt "YouTube Music is a music streaming service by Google"
msgid "Stream music from YouTube Music"
msgstr "यूट्यूब संगीत पर संगीत ढूंढें"

#: main.cpp:76
#, kde-format
msgid "© 2021-2024 Jonah Brüchert, 2021-2024 KDE Community"
msgstr ""

#: main.cpp:77
#, kde-format
msgid "Jonah Brüchert"
msgstr ""

#: main.cpp:77
#, kde-format
msgid "Maintainer"
msgstr ""

#: main.cpp:78
#, kde-format
msgid "Mathis Brüchert"
msgstr ""

#: main.cpp:78
#, kde-format
msgid "Designer"
msgstr ""

#: main.cpp:79
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "राघवेंद्र कामत"

#: main.cpp:79
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "raghu@raghukamath.com"

#: main.cpp:86
#, fuzzy, kde-format
#| msgid "Find music on YouTube Music"
msgctxt "YouTube Music is a music streaming service by Google"
msgid "Unofficial API for YouTube Music"
msgstr "यूट्यूब संगीत पर संगीत ढूंढें"

#: playlistimporter.cpp:22
#, fuzzy, kde-format
#| msgid "Playlists"
msgid "No description"
msgstr "गीत-सूचीयाँ"

#: playlistimporter.cpp:56
#, kde-format
msgid "No title"
msgstr ""

#: playlistimporter.cpp:58
#, kde-format
msgid "No album"
msgstr ""

#, fuzzy
#~| msgid "Playlists"
#~ msgid "Import Playlist from Url"
#~ msgstr "गीत-सूचीयाँ"

#, fuzzy
#~| msgid "Playlists"
#~ msgid "Import Playlist from File"
#~ msgstr "गीत-सूचीयाँ"

#, fuzzy
#~| msgid "Playlists"
#~ msgid "Export Playlist"
#~ msgstr "गीत-सूचीयाँ"

#, fuzzy
#~| msgid "Playlists"
#~ msgid "Import Playlists"
#~ msgstr "गीत-सूचीयाँ"

#, fuzzy
#~| msgid "Playlists"
#~ msgid "Clear Playlist"
#~ msgstr "गीत-सूचीयाँ"

#, fuzzy
#~| msgid "No media playing"
#~ msgid "Now Playing"
#~ msgstr "कोई मीडिया नहीं बजा रहा है"

#~ msgid "Play next"
#~ msgstr "अगला बजाएँ"

#~ msgid "Pause"
#~ msgstr "रोकें"

#~ msgid "Expand"
#~ msgstr "विस्तृत करें"

#~ msgid "Play only Audio"
#~ msgstr "केवल ध्वनी चलाएँ"
